package com.admiraldigital.Movie.service;

import com.admiraldigital.Movie.exception.ResourceNotFoundException;
import com.admiraldigital.Movie.model.Movie;
import com.admiraldigital.Movie.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Service
public class MovieService {
    @Autowired
    MovieRepository movieRepository;

    // Get All Movies
    public List<Movie> getAllMovies() {
        return movieRepository.findAll();
    }

    // Create a new Movie
    public Movie createMovie( Movie movie) {
        return movieRepository.save(movie);
    }

    // Get a movie by id
    public Movie getMovieById( Long movieId) {
        return movieRepository.findById(movieId)
                .orElseThrow(() -> new ResourceNotFoundException("Movie", "id", movieId));
    }

    // Update a Movie
    public Movie updateMovie( Long movieId , Movie movieDetails) {

        Movie movie = movieRepository.findById(movieId)
                .orElseThrow(() -> new ResourceNotFoundException("Movie", "id", movieId));

        movie.setTitle(movieDetails.getTitle());
        movie.setCategory(movieDetails.getCategory());
        movie.setStar_rating(movieDetails.getStar_rating());

        Movie updatedMovie = movieRepository.save(movie);
        return updatedMovie;
    }

    // Delete a Movie
    public ResponseEntity<?> deleteMovie(Long movieId) {
        Movie movie = movieRepository.findById(movieId)
                .orElseThrow(() -> new ResourceNotFoundException("Movie", "id", movieId));

        movieRepository.delete(movie);

        return ResponseEntity.ok().build();
    }
}
