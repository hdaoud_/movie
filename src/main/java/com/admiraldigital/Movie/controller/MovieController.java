package com.admiraldigital.Movie.controller;


import com.admiraldigital.Movie.exception.ResourceNotFoundException;
import com.admiraldigital.Movie.model.Movie;
import com.admiraldigital.Movie.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class MovieController {
    @Autowired
    MovieService movieService;

    // Get All Movies
    @GetMapping("/movies")
    public List<Movie> getAllNotes() {
        return movieService.getAllMovies();
    }

    // Create a new Movie
    @PostMapping("/movies")
    public Movie createNote(@Valid @RequestBody Movie movie) {
        return movieService.createMovie(movie);
    }

    // Get a movie by id
    @GetMapping("/movies/{id}")
    public Movie getMovieById(@PathVariable(value = "id") Long movieId) {
        return movieService.getMovieById(movieId);
    }

    // Update a Movie
    @PutMapping("/movies/{id}")
    public Movie updateMovie(@PathVariable(value = "id") Long movieId , @Valid @RequestBody Movie movieDetails) {
        return movieService.updateMovie(movieId,movieDetails);
    }

    // Delete a Movie
    @DeleteMapping("/movies/{id}")
    public ResponseEntity<?> deleteMovie(@PathVariable(value = "id") Long movieId) {
        return movieService.deleteMovie(movieId);
    }
}
